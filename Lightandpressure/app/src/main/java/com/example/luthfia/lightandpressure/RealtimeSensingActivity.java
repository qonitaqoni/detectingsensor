package com.example.luthfia.lightandpressure;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.security.PrivilegedAction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sofyan on 3/16/2018.
 */

public class RealtimeSensingActivity extends AppCompatActivity {

    EditText inputFrequency;
    EditText inputDuration;
    Button btnRecord;

    ArrayList<Float[]> listAccelero = new ArrayList<Float[]>();

    Float[] statusAcceleroNow=new Float[3];

    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 500);

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime_sensing);

        //init display to view
        final TextView textAcceleroX=(TextView)findViewById(R.id.textAcceleroX);
        final TextView textAcceleroY=(TextView)findViewById(R.id.textAcceleroY);
        final TextView textAcceleroZ=(TextView)findViewById(R.id.textAcceleroZ);
        final TextView textLight=(TextView)findViewById(R.id.textLight);
        final TextView textMagnetic=(TextView)findViewById(R.id.textMagnetic);
        final TextView textOrientation=(TextView)findViewById(R.id.textOrientation);
        final TextView textProximity=(TextView)findViewById(R.id.textProximity);
        final TextView textGyroX=(TextView)findViewById(R.id.textGyroX);
        final TextView textGyroY=(TextView)findViewById(R.id.textGyroY);
        final TextView textGyroZ=(TextView)findViewById(R.id.textGyroZ);
        final TextView textTemperature=(TextView)findViewById(R.id.textTemperature);

        inputFrequency=(EditText)findViewById(R.id.inputFrequency);
        inputDuration=(EditText)findViewById(R.id.inputDuration);
        btnRecord=(Button)findViewById(R.id.btnRecord);

        //setup sensor manager
        SensorManager senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //SETUP ACCELERO
        Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //register sensor
        senSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
                    float x = sensorEvent.values[0];
                    float y = sensorEvent.values[1];
                    float z = sensorEvent.values[2];
                    textAcceleroX.setText("X : "+x);
                    textAcceleroY.setText("Y : "+y);
                    textAcceleroZ.setText("Z : "+z);
                    statusAcceleroNow[0]=x;
                    statusAcceleroNow[1]=y;
                    statusAcceleroNow[2]=z;
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },senAccelerometer,SensorManager.SENSOR_DELAY_NORMAL);

        //SETUP LIGHT
        Sensor senLight = senSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        //register sensor
        senSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent.sensor.getType()==Sensor.TYPE_LIGHT){
                    float val_sensor = sensorEvent.values[0];
                    textLight.setText(val_sensor +"");
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },senLight,SensorManager.SENSOR_DELAY_NORMAL);

        //SETUP MAGNETIC
        Sensor senMagnetic = senSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        //register sensor
        senSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD){
                    textMagnetic.setText(sensorEvent.values[0]+"");
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },senMagnetic,SensorManager.SENSOR_DELAY_NORMAL);

        //SETUP ORIENTATION
        Sensor senOrientation = senSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        //register sensor
        senSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent.sensor.getType()==Sensor.TYPE_ORIENTATION){
                    textOrientation.setText(sensorEvent.values[0]+"");
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },senOrientation,SensorManager.SENSOR_DELAY_NORMAL);

        //SETUP PROXIMITY
        Sensor senProximity = senSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        //register sensor
        senSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent.sensor.getType()==Sensor.TYPE_PROXIMITY){
                    textProximity.setText(sensorEvent.values[0]+"");
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },senProximity,SensorManager.SENSOR_DELAY_NORMAL);

        //SETUP ACCELEROMETER
        Sensor senGyro = senSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        //register sensor
        senSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent.sensor.getType()==Sensor.TYPE_GYROSCOPE){
                    float x = sensorEvent.values[0];
                    float y = sensorEvent.values[1];
                    float z = sensorEvent.values[2];
                    textGyroX.setText("X : "+String.format("%.7f", x));
                    textGyroY.setText("Y : "+String.format("%.7f", y));
                    textGyroZ.setText("Z : "+String.format("%.7f", z));
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },senGyro,SensorManager.SENSOR_DELAY_NORMAL);

        //SETUP TEMPERATURE
        Sensor senTemperature = senSensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
        //register sensor
        senSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent.sensor.getType()==Sensor.TYPE_TEMPERATURE){
                    textTemperature.setText(sensorEvent.values[0]+"");
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },senTemperature,SensorManager.SENSOR_DELAY_NORMAL);


        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int frequency=Integer.valueOf(inputFrequency.getText().toString());
                final int duration=Integer.valueOf(inputDuration.getText().toString());

                Log.e("fre",frequency+"");
                Log.e("fre",duration+"");

                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {

                        }

                        toneGen1.startTone(ToneGenerator.TONE_PROP_BEEP2,150);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //view changes
                                recordStart();
                            }
                        });


                        for(int i=0;i<=duration;i+=frequency) {
                            // Block this thread for 2 seconds.
                            try {
                                Thread.sleep(frequency);
                            } catch (InterruptedException e) {

                            }
                            listAccelero.add(statusAcceleroNow.clone());
                            Log.e("record",statusAcceleroNow[0]+" "+statusAcceleroNow[1]+" "+statusAcceleroNow[2]);

                            // After sleep finished blocking, create a Runnable to run on the UI Thread.
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //view changes

                                }
                            });
                        }

                        toneGen1.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT,150);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //view changes
                                recordEnd();
                            }
                        });

                        Log.e("recorda",listAccelero.toString());
                        String data="";

                        for(int i=0;i<listAccelero.size();i++){
                            data+=i+",";
                            data+=frequency+",";
                            data+=listAccelero.get(i)[0]+",";
                            data+=listAccelero.get(i)[1]+",";
                            data+=listAccelero.get(i)[2]+"\n";
                            Log.e("records",listAccelero.get(i)[0]+" "+listAccelero.get(i)[1]+" "+listAccelero.get(i)[2]);
                        }

                        //write data
                        Date currentTime = Calendar.getInstance().getTime();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(" dd-MM-yyyy HH-mm-ss");
                        String time = simpleDateFormat.format(new Date());

                        writeFileOnInternalStorage(RealtimeSensingActivity.this,"DataSensor"+time+".csv",data);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Record Data Saved!", Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                };
                thread.start();

            }
        });
    }

    //GET STORAGE PERMISSION
    public static void verifyStoragePermissions(Activity activity){
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }
    }

    public void writeFileOnInternalStorage(Context mcoContext,String sFileName, String sBody){
        verifyStoragePermissions(this);
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"Data Read Sensor");
        if(!file.exists()){
            file.mkdir();
        }
        try{
            File gpxfile = new File(file, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            String header = "No."+","+"Frekuensi (ms)"+","+"X"+","+"Y"+","+"Z"+","+"\n";
            writer.append(header);
            writer.append(sBody);
            writer.flush();
            writer.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }



    public void recordStart(){
        Toast.makeText(this, "Recording Data Start!", Toast.LENGTH_SHORT).show();

    }

    public void recordEnd(){
        Toast.makeText(this, "Recording Data Done!", Toast.LENGTH_SHORT).show();

    }






}
