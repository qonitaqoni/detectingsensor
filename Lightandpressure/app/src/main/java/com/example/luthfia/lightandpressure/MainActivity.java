package com.example.luthfia.lightandpressure;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import static java.sql.Types.NULL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SensorEventListener {

    private SensorManager sensorManager;
    private Sensor sensor;

    private Button btn_ambient, btn_light, btn_pressure, btn_humidity, btn_acc;
    private TextView val_ambient, val_light, val_pressure, val_humidity, val_acc;
    private TextView[] val_field = new TextView[5];
    private final int _AMBIENT = 0;
    private final int _LIGHT = 1;
    private final int _PRESSURE = 2;
    private final int _HUMIDITY = 3;
    private final int _ACC = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        btn_ambient = findViewById(R.id.button_ambient);
        btn_light = findViewById(R.id.button_light);
        btn_pressure = findViewById(R.id.button_pressure);
        btn_humidity = findViewById(R.id.button_humidity);
        btn_acc = findViewById(R.id.button_acc);

        btn_ambient.setOnClickListener(this);
        btn_light.setOnClickListener(this);
        btn_pressure.setOnClickListener(this);
        btn_humidity.setOnClickListener(this);
        btn_acc.setOnClickListener(this);

        val_ambient = findViewById(R.id.text_ambient);
        val_field[_AMBIENT] = val_ambient;
        val_light = findViewById(R.id.text_light);
        val_field[_LIGHT] = val_light;
        val_pressure = findViewById(R.id.text_pressure);
        val_field[_PRESSURE] = val_pressure;
        val_humidity = findViewById(R.id.text_humidity);
        val_field[_HUMIDITY] = val_humidity;
        val_acc = findViewById(R.id.text_acc);
        val_field[_ACC] = val_acc;

/*
//        sensorAmbient = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) == null)
            Toast.makeText(this.getApplicationContext(), "There is no ambient sensor in your device!", Toast.LENGTH_SHORT).show();
        else
//            sensorAmbient = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


//        sensorLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) == null)
            Toast.makeText(this.getApplicationContext(), "There is no light sensor in your device!", Toast.LENGTH_SHORT).show();
        else
//            sensorLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


        //        sensorPressure = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) == null)
            Toast.makeText(this.getApplicationContext(), "There is no pressure sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);



//        sensorHumid = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY) == null)
            Toast.makeText(this.getApplicationContext(), "There is no humidity sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


//        sensorAcc = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null)
            Toast.makeText(this.getApplicationContext(), "There is no accelerometer sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);*/


    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float val_sensor = sensorEvent.values[0];
        float val_sensor1 = sensorEvent.values[1];
        float val_sensor2 = sensorEvent.values[2];
        TextView val_curr = val_ambient;
        String info = "";
        int type_curr = sensorEvent.sensor.getType();

        switch (type_curr) {
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
                info = val_sensor + " Celcius";
                val_curr = val_field[_AMBIENT];
                break;
            case Sensor.TYPE_LIGHT:
                info = val_sensor + " SI Lux Units";
                val_curr = val_field[_LIGHT];
                break;
            case Sensor.TYPE_PRESSURE:
                info = val_sensor + " hPa in milibars";
                val_curr = val_field[_PRESSURE];
                break;
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                info = val_sensor + " humidity in percent";
                val_curr = val_field[_HUMIDITY];
                break;
            case Sensor.TYPE_ACCELEROMETER:
                if (val_sensor != NULL) {
                    info = "X: " + val_sensor;

                }
                if (val_sensor1 != NULL) {
                    info = info + " | Y: " + val_sensor1;
                }
                if (val_sensor2 != NULL) {
                    info = info + " | Z: " + val_sensor2;
                }
                val_curr = val_field[_ACC];

                break;
            default:
                break;
        }

        val_curr.setText(info);

        //save battery usage
        sensor = null;
        sensorManager.unregisterListener(this);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        String message_acc = "";
        switch (i) {
            case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
                message_acc = "High Accuracy";
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
                message_acc = "Medium Accuracy";
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
                message_acc = "Low Accuracy";
                break;
            case SensorManager.SENSOR_STATUS_UNRELIABLE:
                message_acc = "Unreliable Accuracy";
                break;
            default:
                break;
        }

        Toast acc_toast = Toast.makeText(this.getApplicationContext(), message_acc, Toast.LENGTH_SHORT);
        acc_toast.show();

    }

    @Override
    public void onClick(View view) {
        /*if (view.getId() == R.id.button_ambient) {
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            if (sensor == null)
                Toast.makeText(this.getApplicationContext(), "There is no ambient sensor in your device!", Toast.LENGTH_SHORT).show();
            else
                sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        } else if (view.getId() == R.id.button_light) {
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
            if (sensor == null)
                Toast.makeText(this.getApplicationContext(), "There is no light sensor in your device!", Toast.LENGTH_SHORT).show();
            else
                sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        } else if (view.getId() == R.id.button_pressure) {
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
            if (sensor == null)
                Toast.makeText(this.getApplicationContext(), "There is no pressure sensor in your device!", Toast.LENGTH_SHORT).show();
            else
                sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


        } else if (view.getId() == R.id.button_humidity) {
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
            if (sensor == null)
                Toast.makeText(this.getApplicationContext(), "There is no humidity sensor in your device!", Toast.LENGTH_SHORT).show();
            else
                sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


        } else if (view.getId() == R.id.button_acc) {
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if (sensor == null)
                Toast.makeText(this.getApplicationContext(), "There is no accelerometer sensor in your device!", Toast.LENGTH_SHORT).show();
            else
                sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


        }*/

        sensing();
    }

    private void sensing() {
        Log.e("sensing","start");
        //TYPE_AMBIENT_TEMPERATURE
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if (sensor == null)
            Toast.makeText(this.getApplicationContext(), "There is no ambient sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


        //TYPE_LIGHT
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (sensor == null)
            Toast.makeText(this.getApplicationContext(), "There is no light sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        //TYPE_PRESSURE
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        if (sensor == null)
            Toast.makeText(this.getApplicationContext(), "There is no pressure sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


        //TYPE_RELATIVE_HUMIDITY
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        if (sensor == null)
            Toast.makeText(this.getApplicationContext(), "There is no humidity sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);


        //TYPE_ACCELEROMETER
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (sensor == null)
            Toast.makeText(this.getApplicationContext(), "There is no accelerometer sensor in your device!", Toast.LENGTH_SHORT).show();
        else
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        Log.e("sensing","finish");
    }


    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
}
